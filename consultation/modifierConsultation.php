<?php
    require('../database.php');
    include("../menu.php");
    include("../verificationConnexion.php");

    $id_consultation = $_GET['Id_consultation'];

    if (isset($_POST['dateDebut'])) {
        $req = $linkpdo->prepare('UPDATE consultation SET dateDebut=?, dateFin=?, Id_medecin=?, Id_usager=? WHERE Id_consultation=?;');
        $test = $req->execute([$_POST['dateDebut'], date('Y-m-d G:i:s', strtotime($_POST['dateDebut'])+$tmp[0]*3600+$tmp[1]*60),$_POST['medecin'],$_POST['patient'], $id_consultation]);
        if(!$test){
            echo "erreur";
        } else {
            header('Location: ' ."affichageConsultation.php", true, 303);
        }
    }


?>

<!doctype html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <title>Modifier Consultation</title>
    <link rel="stylesheet" href="style.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
</head>

