<?php
    require('../database.php');
    include("../menu.php");
    include("../verificationConnexion.php");

    if (!isset($_SESSION['rdv'])){
        header('Location: ' ."../index.php", true, 303);
    }
    $requete = $linkpdo->prepare("SELECT Id_consultation, dateDebut, dateFin, Id_medecin, Id_usager, TIMEDIFF(dateFin, dateDebut) as duree FROM consultation ORDER BY dateDebut desc;");
    $requete->execute();
    $data=$requete->fetchAll();

    $requete = $linkpdo->prepare("SELECT * from usager");
    $requete->execute();
    $donnes=$requete->fetchAll();


?>

<!doctype html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <title>Consultations</title>
    <link rel="stylesheet" href="style.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <script language="JavaScript" type="text/javascript">
        function checkDelete(){
            return confirm('Voulez-vous supprimer?');
        }
    </script>
</head>
<body>
    <form action="saisieConsultation.php" method="get">
        <div class="container">
            <div class="row">
                <div class="col-md-4"></div>
                <div class="col-md-4">
                    <div class="mb-3">
                        <label for="patient" class="form-label">Patient</label>
                        <select class="form-select form-select-sm" name="patient" id="patient" aria-label=".form-select-sm example" required>
                            <?php
                            foreach($donnes as $row) {
                                echo " <option value='" . $row['Id_usager'] . "'> " . $row['nom'] . "  " . $row['prenom'] . " </option> ";
                            }
                            ?>
                        </select>
                    </div>
                    <button type="submit" class="btn btn-primary">Ajouter</button>
                </div>
            </div>
        </div>
    </form>

    <table style="border: 1px solid #333;" class="table">
        <thead>
            <tr>
                <th scope="col">Patient</th>
                <th scope="col">Heure début</th>
                <th scope="col">Durée</th>
                <th scope="col">Médecin</th>
                <th scope="col">Action</th>
            </tr>
        </thead>
        <?php
        foreach($data as $row) {
            $requete = $linkpdo->prepare("SELECT * FROM medecin where Id_medecin=" . $row['Id_medecin'] . ";");
            $requete->execute();
            $medecin = $requete->fetchAll();
            if (isset($medecin[0])) {
                $medecin = $medecin[0];
                if ($medecin['civilite'] == "H") {
                    $medecin['civilite'] = "Mr";
                }
                if ($medecin['civilite'] == "F") {
                    $medecin['civilite'] = "Mme";
                }
                if ($medecin['civilite'] == "A") {
                    $medecin['civilite'] = "";
                }
            } else {
                $medecin = null;
            }

            if ($medecin != null) {
                $med = $medecin['civilite'] . ". " . $medecin['nom'] . " " . $medecin['prenom'];
            } else {
                $med = "Pas de médecin référent";
            }

            $requete = $linkpdo->prepare("SELECT * FROM usager where Id_usager=" . $row['Id_usager'] . ";");
            $requete->execute();
            $usager = $requete->fetchAll();
            if (isset($usager[0])) {
                $usager = $usager[0];
                if ($usager['civilite'] == "H") {
                    $usager['civilite'] = "Mr";
                }
                if ($usager['civilite'] == "F") {
                    $usager['civilite'] = "Mme";
                }
                if ($usager['civilite'] == "A") {
                    $usager['civilite'] = "";
                }
            }

            if ($usager != null) {
                $usa = $usager['civilite'] . ". " . $usager['nom'] . " " . $usager['prenom'];
            }

            echo "
    
                <tr>
                    <td>$usa</td>
                    <td>" . $row['dateDebut'] . "</td>
                    <td>" . $row['duree'] . "</td>
                    <td>$med</td>
                    <td>
                        <a type='button' class='btn btn-secondary' href='modifierConsultation.php?Id_consultation=" . $row['Id_consultation'] . "'>Modifier</a>
                        <a type='button' class='btn btn-danger' href='supprimerConsultation.php?Id_consultation=" . $row['Id_consultation'] . "' onclick='return checkDelete()'>Supprimer</a>
                    </td>
                </tr>";

        }
        ?>
        </tbody>
    </table>
</body>
