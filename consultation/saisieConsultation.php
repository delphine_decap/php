<?php
    require('../database.php');
    include("../menu.php");
    include("../verificationConnexion.php");


    $id_usager = $_GET['patient'];

    $req1 = $linkpdo->prepare('SELECT * FROM usager WHERE Id_usager=?');
    $req1->execute([$id_usager]);
    $ref = $req1->fetchAll()[0];

    if(isset($_POST['patient'])){
        $tmp = explode(':',$_POST['duree']);
        $requete = $linkpdo->prepare("INSERT INTO consultation( dateDebut, dateFin, Id_medecin, Id_usager) VALUES(?,?,?,?)");
        $test=$requete->execute([$_POST['dateDebut'], date('Y-m-d G:i:s', strtotime($_POST['dateDebut'])+$tmp[0]*3600+$tmp[1]*60),$_POST['medecin'],$_POST['patient']]);
        if(!$test){
            echo "erreur";
        } else {
            header('Location: ' ."affichageConsultation.php", true, 303);
        }
    }

    $medecins = $linkpdo->prepare("select * from medecin");
    $medecins->execute();
    $donnees=$medecins->fetchAll();

    $usagers = $linkpdo->prepare("select * from usager");
    $usagers->execute();
    $data=$usagers->fetchAll();
?>


<!doctype html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <title>Ajouter Consultation</title>
    <link rel="stylesheet" href="style.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
</head>

<body>
<form action="saisieConsultation.php?patient=<?php echo $id_usager ?>" method="post">
    <div class="container">
        <div class="row">
            <div class="col-md-4"></div>
            <div class="col-md-4">

                <div class="mb-3">
                    <label for="patient" class="form-label">Patient</label>
                    <select class="form-select form-select-sm" name="patient" id="patient" aria-label=".form-select-sm example">
                        <option value=null>Aucun patient sélectionné</option>
                        <?php
                        foreach($data as $row) {
                            if ($row['Id_usager']==$id_usager){
                                echo " <option value='" . $row['Id_usager'] . "' selected> " . $row['nom'] . "  " . $row['prenom'] . " </option> ";
                            }else{
                                echo " <option value='" . $row['Id_usager'] . "'> " . $row['nom'] . "  " . $row['prenom'] . " </option> ";
                            }
                        }
                        ?>
                    </select>
                </div>

                <div class="mb-3">
                    <label for="medecin" class="form-label">Medecin</label>
                    <select class="form-select form-select-sm" name="medecin" id="medecin" aria-label=".form-select-sm example">
                        <?php
                        foreach($donnees as $row) {
                            if ($ref['Id_medecin']==$row['Id_medecin']){
                                echo " <option value='" . $row['Id_medecin'] . "' selected> " . $row['nom'] . "  " . $row['prenom'] . " </option> ";
                            } else {
                                echo " <option value='" . $row['Id_medecin'] . "'> " . $row['nom'] . "  " . $row['prenom'] . " </option> ";
                            }
                        }
                        ?>
                    </select>
                </div>


                <div class="mb-3">
                    <label for="dateDebut" class="form-label">Date et heure du rendez-vous</label>
                    <input type="datetime-local" class="form-control" name="dateDebut" id="dateDebut" required>
                </div>

                <div class="mb-3">
                    <label for="duree" class="form-label">Duree du rendez-vous</label>
                    <input type="time" class="form-control" name="duree" id="duree" required value="00:30">
                </div>


                <button type="submit" class="btn btn-primary">Valider</button>
                <a type='button' class='btn btn-secondary' href="affichageConsultation.php">Retour</a></div>

            <div class="col-md-4"></div>
        </div>
    </div>
</form>
</body>




