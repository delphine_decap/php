<?php
    require('../database.php');
    include("../menu.php");
    include("../verificationConnexion.php");

    if (!isset($_SESSION['rdv'])){
        header('Location: ' ."../index.php", true, 303);
    }

    $req1 = $linkpdo->prepare('SELECT count(*) FROM usager WHERE DATEDIFF(current_date(),dateNaissance)<25*365 and civilite="F"');
    $req1->execute();
    $f25 = $req1->fetchAll()[0];

    $req2 = $linkpdo->prepare('SELECT count(*) FROM usager WHERE DATEDIFF(current_date(),dateNaissance)<25*365 and civilite="H"');
    $req2->execute();
    $m25 = $req2->fetchAll()[0];

    $req3 = $linkpdo->prepare('SELECT count(*) FROM usager WHERE DATEDIFF(current_date(),dateNaissance)>=25*365 and DATEDIFF(current_date(),dateNaissance)<50*365 and civilite="F"');
    $req3->execute();
    $f2550 = $req3->fetchAll()[0];

    $req4 = $linkpdo->prepare('SELECT count(*) FROM usager WHERE DATEDIFF(current_date(),dateNaissance)>=25*365 and DATEDIFF(current_date(),dateNaissance)<50*365 and civilite="H"');
    $req4->execute();
    $m2550 = $req4->fetchAll()[0];

    $req5 = $linkpdo->prepare('SELECT count(*) FROM usager WHERE DATEDIFF(current_date(),dateNaissance)>=50*365 and civilite="F"');
    $req5->execute();
    $f50 = $req5->fetchAll()[0];

    $req6 = $linkpdo->prepare('SELECT count(*) FROM usager WHERE DATEDIFF(current_date(),dateNaissance)>=50*365 and civilite="H"');
    $req6->execute();
    $m50 = $req6->fetchAll()[0];

    $req7 = $linkpdo->prepare('SELECT nom, prenom, Id_medecin FROM medecin');
    $req7->execute();
    $data = $req7->fetchAll();

?>

<!doctype html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <title>Statistiques</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
</head>

<style>
    table {border-collapse:collapse;}
    td {border: 1px solid #333333;padding: 20px ;text-align:center;}
    tr {border: 1px solid #333333;padding: 20px ;text-align:center;}
    th {border: 1px solid #333333;padding: 20px ;text-align:center;}
</style>

<body>
<form action="statistique.php" method="get">
    <table>
        <thead>
        <tr>
            <th>Tranche d'âge</th>
            <th>Nombre d'hommes</th>
            <th>Nombre de femmes</th>
        </tr>
        </thead>

        <?php
        echo"
                <tbody>
                <tr>
                    <th>Moins de 25 ans</th>
                    <td>".$m25[0]."</td>
                    <td>".$f25[0]."</td>
                </tr>
                <tr>
                    <th>Entre 25 et 50 ans</th>
                    <td>".$m2550[0]."</td>
                    <td>".$f2550[0]."</td>
                </tr>
                <tr>
                    <th>Plus de 50 ans</th>
                    <td>".$m50[0]."</td>
                    <td>".$f50[0]."</td>
                </tr>
                </tbody> "
        ?>
    </table>


    <table style="border: 1px solid #333;" class="table">
        <thead>
        <tr>
            <th scope="col">Medecin</th>
            <th scope="col">Nombre d'heures effectuées </th>
        </tr>
        </thead>

        <?php

        foreach($data as $donnees) {
            $req8 = $linkpdo->prepare('SELECT dateFin, dateDebut FROM consultation WHERE Id_medecin=?');
            $req8->execute([$donnees['Id_medecin']]);
            $result = $req8->fetchAll();
            $nbs=0;
            foreach($result as $cons){
                $nbs=$nbs+(strtotime($cons[0])-strtotime($cons[1]));
            }

            echo "
    
                    <tr>
                        <td>" . $donnees['nom'] . " " . $donnees['prenom'] . "</td>
                        <td>" . date("H:i:s",$nbs) . " </td>
                    </tr> ";
        }
        ?>
    </table>
</form>
</body>
