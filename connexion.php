<?php
    require('database.php');
    include("menu.php");

    if (isset($_SESSION['rdv'])){
        header('Location: ' ."consultation/affichageConsultation.php", true, 303);
    }

    if (isset($_POST["login"])){
        $requete = $linkpdo->prepare("SELECT * FROM utilisateur WHERE login=?;");
        $requete->execute([$_POST['login']]);
        $data=$requete->fetchAll()[0];

        if (isset($data)){
            if (md5($_POST['mdp'])==$data['password']){
                $_SESSION["rdv"]=$_POST["login"];
                header('Location: ' ."consultation/affichageConsultation.php", true, 303);
            } else {
                echo "Mot de passe incorrect, veuillez réessayer"."<br>";
            }
        } else {
            echo "Utilisateur introuvable"."<br>";
        }
    }
?>

<!doctype html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <title>Ajouter Usager</title>
    <link rel="stylesheet" href="style.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
</head>
<body>
<form action="connexion.php" method="post">
    <div class="container">
        <div class="row">
            <div class="col-md-4"></div>
            <div class="col-md-4">
                <br>
                <p>Entrez vos identifiants de connexion</p>
                <div class="mb-3">
                    <label for="login" class="form-label">Login</label>
                    <input type="text" class="form-control" name="login" id="login" required>
                </div>

                <div class="mb-3">
                    <label for="mdp" class="form-label">Mot de passe</label>
                    <input type="password" class="form-control" name="mdp" id="mdp" required>
                </div>

                <button type="submit" class="btn btn-primary">Valider</button>
                <a type='button' class='btn btn-secondary' href="index.php">Retour</a></div>

            <div class="col-md-4"></div>
        </div>
    </div>
</form>
</body>

