<?php
require('../database.php');
include("../menu.php");
include("../verificationConnexion.php");

if (isset($_POST["nom"])){
    $verif = $linkpdo->prepare("SELECT COUNT(*) FROM medecin WHERE nom=? AND prenom=? AND civilite=?;");
    $verif->execute([$_POST['nom'],$_POST['prenom'],$_POST['civilite']]);
    $data=$verif->fetchAll()[0][0];
    if ($data==0){
        $requete = $linkpdo->prepare("INSERT INTO medecin(civilite, nom, prenom) VALUES(?,?,?)");
        $test=$requete->execute([$_POST['civilite'],$_POST['nom'],$_POST['prenom']]);
        if(!$test){
            echo "erreur";
        } else {
            header('Location: ' ."medecins.php", true, 303);
        }
    }
}
?>

<!doctype html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <title>Ajout Medecin</title>
    <link rel="stylesheet" href="style.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
</head>
<body>
<form action="ajouterMedecin.php" method="post">
    <div class="container">
        <div class="row">
            <div class="col-md-4"></div>
            <div class="col-md-4">
                <div class="mb-3">
                    <label for="nommedecin" class="form-label">Nom</label>
                    <input type="text" class="form-control" name="nom" id="nommedecin" required>
                </div>

                <div class="mb-3">
                    <label for="prenommedecin" class="form-label">Prénom</label>
                    <input type="text" class="form-control" name="prenom" id="prenommedecin" required>
                </div>

                <p>Civilité</p>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="civilite" id="homme" value="H">
                    <label class="form-check-label" for="homme">M</label>
                </div>

                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="civilite" id="femme" value="F" checked>
                    <label class="form-check-label" for="femme">F</label>
                </div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="civilite" id="autre" value="A">
                    <label class="form-check-label" for="autre">Autre</label>
                </div>

                <div> <br> </div>

                <button type="submit" class="btn btn-primary">Valider</button>
                <a type='button' class='btn btn-secondary' href="medecins.php">Retour</a></div>

            <div class="col-md-4"></div>
        </div>
    </div>
</form>
</body>
