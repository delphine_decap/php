<?php
require('../database.php');
include("../menu.php");
include("../verificationConnexion.php");

$id_medecin = $_GET['Id_medecin'];

if (isset($_POST['nom'])) {
    $req = $linkpdo->prepare('UPDATE medecin SET civilite=?, nom=?, prenom=? WHERE Id_medecin=?;');
    $test = $req->execute([$_POST['civilite'], $_POST['nom'], $_POST['prenom'], $id_medecin]);
    if(!$test){
        echo "erreur";
    } else {
        header('Location: ' ."medecins.php", true, 303);
    }
}

$req1 = $linkpdo->prepare('SELECT * FROM medecin WHERE Id_medecin=?');
$req1->execute([$id_medecin]);
$data = $req1->fetchAll()[0];
?>

<!doctype html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <title>Modifier Medecin</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
</head>

<form action="modifierMedecin.php?Id_medecin=<?php echo $id_medecin ?>" method="post">
    <div class="container">
        <div class="row">
            <div class="col-md-4"></div>
            <div class="col-md-4">

                <div class="mb-3">
                    <label for="nom" class="form-label" >Nom</label>
                    <input type="text" class="form-control" name="nom" id="nom" value="<?php echo $data['nom'] ?>" required>
                </div>

                <div class="mb-3">
                    <label for="prenom" class="form-label"  >Prénom</label>
                    <input type="text" class="form-control" name="prenom" id="prenom" value="<?php echo $data['prenom'] ?>" required>
                </div>

                <p>Civilité</p>

                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="civilite" id="civilite" value="H" <?php if($data['civilite'] == 'H') echo "checked"; ?>>
                    <label class="form-check-label" for="inlineRadio1">M</label>
                </div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="civilite" id="civilite" value="F" <?php if($data['civilite'] == 'F') echo "checked"; ?>>
                    <label class="form-check-label" for="inlineRadio2">F</label>
                </div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="civilite" id="civilite" value="A" <?php if($data['civilite'] == 'A') echo "checked"; ?>>
                    <label class="form-check-label" for="inlineRadio3">Autre </label>
                </div>

                <button type="submit" class="btn btn-primary">Valider</button>
                <a type='button' class='btn btn-secondary' href="medecins.php">Retour</a></div>

            <div class="col-md-4"></div>
        </div>
    </div>
</form>

