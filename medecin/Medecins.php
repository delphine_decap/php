<?php

require('../database.php');
include("../menu.php");
include("../verificationConnexion.php");

if (!isset($_SESSION['rdv'])){
    header('Location: ' ."../index.php", true, 303);
}

$requete =  $linkpdo->prepare("SELECT * FROM medecin;");
$requete->execute();
$data=$requete->fetchAll();
?>
<!doctype html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <title>Medecins</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <script language="JavaScript" type="text/javascript">
        function checkDelete(){
            return confirm('Voulez vous vraiment supprimer le medecin?');
        }
    </script>
</head>

<body>
<a type="button" class="btn btn-primary" href='ajouterMedecin.php' >Ajouter</a>
<table style="border: 1px solid #333;" class="table">
    <thead>
    <tr>
        <th scope="col">Civilité</th>
        <th scope="col">Nom</th>
        <th scope="col">Prénom</th>
    </tr>
    </thead>
    <tbody>
    <?php
    foreach($data as $row){
        if($row['civilite']=='H'){
            $row['civilite']='Monsieur';
        }
        if($row['civilite']=='F'){
            $row['civilite']='Madame';
        }
        if($row['civilite']=='A'){
            $row['civilite']='Non défini';
        }
        echo "
                    <tr>
                        <td>".$row['civilite']."</td>
                        <td>".$row['nom']."</td>
                        <td>".$row['prenom']."</td>
                        <td>
                            <a type='button' class='btn btn-danger' href='supprimerMedecin.php?Id_medecin=".$row['Id_medecin']."' onclick='return checkDelete()'> Supprimer</a>
                            <a type='button' class='btn btn-secondary' href='modifierMedecin.php?Id_medecin=".$row['Id_medecin']."'>Modifier</a>
                 
                        </td>
                        
                    </tr>";
    }
    ?>
    </tbody>
</table>