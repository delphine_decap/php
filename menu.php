<?php
    session_start();
?>
<!doctype html>
<html lang='fr'>
<head>
    <meta charset='utf-8'>
        <link href='https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css' rel='stylesheet' integrity='sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3' crossorigin='anonymous'>
</head>
            
    <nav class='navbar navbar-light' style='background-color: #e3f2fd;'>
        <div class='container-fluid'>
            <a class='navbar-brand' href='../index.php'>Accueil</a>
            <a class='navbar-brand' href='../usager/Usagers.php'>Patients</a>
            <a class='navbar-brand' href='../medecin/Medecins.php'>Medecins</a>
            <a class='navbar-brand' href='../consultation/affichageConsultation.php'>Consultations</a>
            <a class='navbar-brand' href='../statistique/statistique.php'>Statistiques</a>
        </div>
    </nav>
