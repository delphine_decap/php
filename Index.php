<?php
    require('database.php');
    include("menu.php");

?>

<!doctype html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <title>Ajouter Usager</title>
    <link rel="stylesheet" href="style.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
</head>
<body>
    <form action="connexion.php" method="post">
        <div class="container">
            <div class="row">
                <div class="col-md-4"></div>
                <div class="col-md-4">
                    <br>
                    <p>Bienvenue sur l'application du cabinet medical</p>
                    <?php
                    if (isset($_SESSION["rdv"])){
                        echo "<a type='button' class='btn btn-secondary' href='deconnexion.php'>Deconnexion</a>";
                    } else{
                        echo "<a type='button' class='btn btn-primary' href='connexion.php'>Connexion</a>";
                    }
                    ?>
                </div>
            </div>
        </div>
    </form>
</body>