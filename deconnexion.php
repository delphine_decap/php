<?php
    require('database.php');
    include("menu.php");
    session_destroy();
    unset($_SESSION["rdv"]);
    header('Location: ' ."index.php", true, 303);