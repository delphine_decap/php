<?php
    require('../database.php');
    include("../menu.php");
    include("../verificationConnexion.php");
    if (isset($_POST["nom"])){
        $verif = $linkpdo->prepare("SELECT COUNT(*) FROM usager WHERE nom=? AND prenom=? AND numSecu=?;");
        $verif->execute([$_POST['nom'],$_POST['prenom'],$_POST['secu']]);
        $data=$verif->fetchAll()[0][0];
        if ($data==0){
            $requete = $linkpdo->prepare("INSERT INTO usager(civilite, nom, prenom, adresse, codePostal, ville, dateNaissance, lieuNaissance, numSecu, Id_medecin) VALUES(?,?,?,?,?,?,?,?,?,?)");
            $test=$requete->execute([$_POST['civilite'],$_POST['nom'],$_POST['prenom'],$_POST['adresse'],$_POST['codepostal'],$_POST['ville'],$_POST['datenaissance'],$_POST['lieunaissance'],$_POST['secu'], $_POST['medecin']]);
            if(!$test){
                echo "erreur dans un des champs";
            } else {
                header('Location: ' ."usagers.php", true, 303);
            }
        }
    }

    $medecins = $linkpdo->prepare("select * from medecin");
    $medecins->execute();
    $donnees=$medecins->fetchAll();

?>

<!doctype html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <title>Ajouter Usager</title>
    <link rel="stylesheet" href="style.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
</head>
<body>
<form action="ajouterUsager.php" method="post">
    <div class="container">
        <div class="row">
            <div class="col-md-4"></div>
            <div class="col-md-4">
                <div class="mb-3">
                    <label for="nomusager" class="form-label">Nom</label>
                    <input type="text" class="form-control" name="nom" id="nomusager" required>
                </div>

                <div class="mb-3">
                    <label for="prenomusager" class="form-label">Prénom</label>
                    <input type="text" class="form-control" name="prenom" id="prenomusager" required>
                </div>

                <p>Civilité</p>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="civilite" id="homme" value="H">
                    <label class="form-check-label" for="homme">M</label>
                </div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="civilite" id="femme" value="F" checked>
                    <label class="form-check-label" for="femme">F</label>
                </div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="civilite" id="autre" value="A">
                    <label class="form-check-label" for="autre">Autre</label>
                </div>

                <div class="mb-3">
                    <label for="adresse" class="form-label">Adresse</label>
                    <input type="text" class="form-control" name="adresse" id="adresse">
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="mb-3">
                            <label for="codepostal" class="form-label">Code postal</label>
                            <input type="text" class="form-control" name="codepostal" id="codepostal">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="mb-3">
                            <label for="ville" class="form-label">Ville</label>
                            <input type="text" class="form-control" name="ville" id="ville">
                        </div>
                    </div>
                </div>

                <div class="mb-3">
                    <label for="datenaissance" class="form-label">Date de naissance</label>
                    <input type="date" class="form-control" name="datenaissance" id="datenaissance" required>
                </div>

                <div class="mb-3">
                    <label for="lieunaissance" class="form-label">Lieu de naissance</label>
                    <input type="text" class="form-control" name="lieunaissance" id="lieunaissance" required>
                </div>

                <div class="mb-3">
                    <label for="secu" class="form-label">N° Sécurité sociale</label>
                    <input type="number" class="form-control" name="secu" id="secu">
                </div>

                <div class="mb-3">
                    <label for="medecin" class="form-label">Medecin référent</label>
                    <select class="form-select form-select-sm" name="medecin" id="medecin" aria-label=".form-select-sm example">
                        <option value=null>Vous n'avez pas de medecin référent</option>
                        <?php
                        foreach($donnees as $row) {
                            echo " <option value='" . $row['Id_medecin'] . "'> " . $row['nom'] . "  " . $row['prenom'] . " </option> ";
                        }
                        ?>
                    </select>
                </div>

                <button type="submit" class="btn btn-primary">Valider</button>
                <a type='button' class='btn btn-secondary' href="usagers.php">Retour</a></div>

            <div class="col-md-4"></div>
        </div>
    </div>
</form>
</body>
