<?php
    require('../database.php');
    include("../menu.php");
    include("../verificationConnexion.php");
    $id_usager = $_GET['Id_usager'];

    if (isset($_POST['nom'])) {
        $req = $linkpdo->prepare('UPDATE usager SET civilite=?, nom=?, prenom=?, adresse=?, codePostal=?, ville=?, dateNaissance=?, lieuNaissance=?, numSecu=?, Id_medecin=? WHERE Id_usager=?;');
        $test = $req->execute([$_POST['civilite'], $_POST['nom'], $_POST['prenom'], $_POST['adresse'], $_POST['codePostal'], $_POST['ville'], $_POST['dateNaissance'], $_POST['lieuNaissance'], $_POST['numSecu'], $_POST['Id_medecin'], $id_usager]);
        if(!$test){
            echo "erreur";
        } else {
            header('Location: ' ."usagers.php", true, 303);
        }
    }

    $req1 = $linkpdo->prepare('SELECT * FROM usager WHERE Id_usager=?');
    $req1->execute([$id_usager]);
    $data = $req1->fetchAll()[0];

    $medecins = $linkpdo->prepare("select * from medecin");
    $medecins->execute();
    $donnees=$medecins->fetchAll();
?>

<!doctype html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <title>Modifier Usager</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
</head>

<form action="modifierUsager.php?Id_usager=<?php echo $id_usager ?>" method="post">
    <div class="container">
        <div class="row">
            <div class="col-md-4"></div>
            <div class="col-md-4">

                <div class="mb-3">
                    <label for="nom" class="form-label" >Nom</label>
                    <input type="text" class="form-control" name="nom" id="nom" value="<?php echo $data['nom'] ?>" required>
                </div>

                <div class="mb-3">
                    <label for="prenom" class="form-label"  >Prénom</label>
                    <input type="text" class="form-control" name="prenom" id="prenom" value="<?php echo $data['prenom'] ?>" required>
                </div>

                <p>Civilité</p>

                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="civilite" id="civilite" value="H" <?php if($data['civilite'] == 'H') echo "checked"; ?>>
                    <label class="form-check-label" for="inlineRadio1">M</label>
                </div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="civilite" id="civilite" value="F" <?php if($data['civilite'] == 'F') echo "checked"; ?>>
                    <label class="form-check-label" for="inlineRadio2">F</label>
                </div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="civilite" id="civilite" value="A" <?php if($data['civilite'] == 'A') echo "checked"; ?>>
                    <label class="form-check-label" for="inlineRadio3">Autre </label>
                </div>


                <div class="mb-3">
                    <label for="adresse" class="form-label" >Adresse</label>
                    <input type="text" class="form-control" name="adresse" id="adresse" value="<?php echo $data['adresse'] ?>">
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="mb-3">
                            <label for="codePostal" class="form-label" >Code Postal</label>
                            <input type="text" class="form-control" name="codePostal" id="codePostal" value="<?php echo $data['codePostal'] ?>">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="mb-3">
                            <label for="ville" class="form-label" >Ville</label>
                            <input type="text" class="form-control" name="ville" id="ville" value="<?php echo $data['ville'] ?>">
                        </div>
                    </div>
                </div>

                <div class="mb-3">
                    <label for="dateNaissance" class="form-label" >Date naissance</label>
                    <input type="date" class="form-control" name="dateNaissance" id="dateNaissance" value="<?php echo $data['dateNaissance'] ?>" required>
                </div>

                <div class="mb-3">
                    <label for="lieuNaissance" class="form-label" >Lieu de naissance</label>
                    <input type="text" class="form-control" name="lieuNaissance" id="lieuNaissance" value="<?php echo $data['lieuNaissance'] ?>" required>
                </div>

                <div class="mb-3">
                    <label for="numSecu" class="form-label" >Numéro de sécurité sociale</label>
                    <input type="text" class="form-control" name="numSecu" id="numSecu" value="<?php echo $data['numSecu'] ?>">
                </div>

                <div class="mb-3">
                    <label for="medecin" class="form-label">Medecin référent</label>
                    <select class="form-select form-select-sm" name="medecin" id="medecin" aria-label=".form-select-sm example">
                        <option value=null>Vous n'avez pas de medecin référent</option>
                        <?php
                        foreach($donnees as $row) {
                            if ($data['Id_medecin']==$row['Id_medecin']){
                                echo " <option value='" . $row['Id_medecin'] . "' selected> " . $row['nom'] . "  " . $row['prenom'] . " </option> ";
                            }else {
                                echo " <option value='" . $row['Id_medecin'] . "'> " . $row['nom'] . "  " . $row['prenom'] . " </option> ";
                            }
                        }
                        ?>
                    </select>
                </div>

                <button type="submit" class="btn btn-primary">Valider</button>
                <a type='button' class='btn btn-secondary' href="usagers.php">Retour</a></div>

            <div class="col-md-4"></div>
        </div>
    </div>
</form>

