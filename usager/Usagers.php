<?php
    require('../database.php');
    include("../menu.php");
    include("../verificationConnexion.php");

    $requete = $linkpdo->prepare("SELECT * FROM usager;");
    $requete->execute();
    $data=$requete->fetchAll();
    if (!isset($_SESSION['rdv'])){
        header('Location: ' ."../index.php", true, 303);
    }
?>

<!doctype html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <title>Usagers</title>
    <link rel="stylesheet" href="style.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <script language="JavaScript" type="text/javascript">
        function checkDelete(){
            return confirm('Voulez-vous supprimer?');
        }
    </script>
</head>
<body>
<a type="button" class="btn btn-primary" href="ajouterUsager.php">Ajouter</a>
<table style="border: 1px solid #333;" class="table">
    <thead>
    <tr>
        <th scope="col">Nom</th>
        <th scope="col">Prenom</th>
        <th scope="col">Adresse</th>
        <th scope="col">Code Postal</th>
        <th scope="col">Ville</th>
        <th scope="col">Medecin</th>
        <th scope="col">Action</th>
    </tr>
    </thead>
    <tbody>
    <?php
    foreach($data as $row){
        $requete = $linkpdo->prepare("SELECT * FROM medecin where Id_medecin=".$row['Id_medecin'].";");
        $requete->execute();
        $medecin=$requete->fetchAll();
        if (isset($medecin[0])){
            $medecin=$medecin[0];
            if($medecin['civilite'] == "H"){
                $medecin['civilite'] = "Mr";
            }
            if($medecin['civilite'] == "F"){
                $medecin['civilite'] = "Mme";
            }
            if($medecin['civilite'] == "A"){
                $medecin['civilite'] = "";
            }
        } else {
            $medecin=null;
        }

        if ($medecin != null){
            $med=$medecin['civilite'].". ".$medecin['nom']." ".$medecin['prenom'];
        } else {
            $med="Pas de médecin référent";
        }
        echo "

                    <tr>
                        <td>".$row['nom']."</td>
                        <td>".$row['prenom']."</td>
                        <td>".$row['adresse']."</td>
                        <td>".$row['codePostal']."</td>
                        <td>".$row['ville']."</td>
                        <td>$med</td>
                        <td>
                            <a type='button' class='btn btn-secondary' href='modifierUsager.php?Id_usager=".$row['Id_usager']."'>Modifier</a>
                            <a type='button' class='btn btn-danger' href='supprimerUsager.php?Id_usager=".$row['Id_usager']."' onclick='return checkDelete()'>Supprimer</a>
                        </td>
                    </tr>";
    }
    ?>
    </tbody>
</table>
</body>